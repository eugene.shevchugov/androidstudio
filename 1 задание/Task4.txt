import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Task4 {
    public static void main(String[] args) throws Exception {
        // ������ ������� � ���
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(bufferedReader.readLine());
        while (a != 1 && a % 2 == 0) {
            a /= 2;
        }
        System.out.println(a == 1 ? "Yes" : "No");
    }
}
