package com.example.list;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        WebView webView = findViewById(R.id.webView);
        Intent intent = getIntent();
        ImageView imageView = new ImageView(this);

        String resName = "c" + intent.getIntExtra("title", 0);
        if (resName.equals("c0")) {
            setContentView(R.layout.car);
        }
        else if (resName.equals("c1")) {
            setContentView(R.layout.toyota);
        }

        else if (resName.equals("c2")) {
            setContentView(R.layout.honda);
        }

        else if (resName.equals("c3")) {
            setContentView(R.layout.ford);
        }

        else if (resName.equals("c4")) {
            setContentView(R.layout.mazda);
        }

        Log.i("name", resName);
        Context context = getBaseContext(); //получаем контекст
        //читаем текстовый файл из ресурсов по имени
        String text = readRawTextFile(context, getResources().getIdentifier(resName,
                "raw", "com.example.list"));

        webView.loadDataWithBaseURL(null, text, "text/html", "en_US", null);
    }

    //читаем текст из raw-ресурсов
    private String readRawTextFile(Context context, int resId)
    {
        InputStream inputStream = context.getResources().openRawResource(resId);

        InputStreamReader inputReader = new InputStreamReader(inputStream);
        BufferedReader buffReader = new BufferedReader(inputReader);
        String line;
        StringBuilder builder = new StringBuilder();

        try {
            while (( line = buffReader.readLine()) != null) {
                builder.append(line);
                builder.append("\n");
            }
        } catch (IOException e) {
            return null;
        }
        return builder.toString();
    }
}
