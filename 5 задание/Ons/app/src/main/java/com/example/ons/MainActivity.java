package com.example.ons;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private String status = "Life cycle";
    private TextView textViewInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewInfo = (TextView) findViewById(R.id.textInfo);
        Toast.makeText(getApplicationContext(), "Метод onCreate() отработал", Toast.LENGTH_LONG).show();
        Log.i(status, "onCreate()");
    }
    @Override
    protected void onStart() {
        long startTime = System.nanoTime();
        super.onStart();
        long estimateTime = System.nanoTime() - startTime;
        String time = Long.toString(estimateTime / 1000);
        Toast.makeText(getApplicationContext(), "Метод onStart() отработал. Время - " + time + " мс", Toast.LENGTH_LONG).show();
        Log.i(status, "onStart()");
    }
    @Override
    protected void onResume() {
        long startTime = System.nanoTime();
        super.onResume();
        long estimateTime = System.nanoTime() - startTime;
        String time = Long.toString(estimateTime / 1000);
        Toast.makeText(getApplicationContext(), "Метод onResume() отработал. Время - " + time + " мс", Toast.LENGTH_LONG).show();
        Log.i(status, "onResume()");
    }
    @Override
    protected void onPause() {
        long startTime = System.nanoTime();
        super.onPause();
        long estimateTime = System.nanoTime() - startTime;
        String time = Long.toString(estimateTime / 1000);
        Toast.makeText(getApplicationContext(), "Метод onPause() отработал. Время - " + time + " мс", Toast.LENGTH_LONG).show();
        Log.i(status, "onPause()");
    }
    @Override
    protected void onStop() {
        long startTime = System.nanoTime();
        super.onStop();
        long estimateTime = System.nanoTime() - startTime;
        String time = Long.toString(estimateTime / 1000);
        Toast.makeText(getApplicationContext(), "Метод onStop() отработал. Время - " + time + " мс", Toast.LENGTH_LONG).show();
        Log.i(status, "onStop()");
    }
    @Override
    protected void onRestart() {
        long startTime = System.nanoTime();
        super.onRestart();
        long estimateTime = System.nanoTime() - startTime;
        String time = Long.toString(estimateTime / 1000);
        Toast.makeText(getApplicationContext(), "Метод onRestart() отработал. Время - " + time + " мс", Toast.LENGTH_LONG).show();
        Log.i(status, "onRestart()");
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonTouch:
                textViewInfo.setText("Вы уже запустили приложение");
                break;
            case R.id.buttonExit:
                finish();
                break;
            default:
                break;
        }
    }
}
