package com.example.graphicbubble;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.view.View;

import java.util.ArrayList;
import java.util.Random;

public class BubbleView extends ImageView implements View.OnTouchListener {
    private Random rand = new Random();
    private ArrayList<Bubble> bubbleList;
    private int size  = 50;
    private int delay = 33;
    private Paint paint = new Paint();
    private Handler handler = new Handler();

    public BubbleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        bubbleList = new ArrayList<Bubble>();
        //testBubbles();
        setOnTouchListener(this);
    }
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            /*for (Bubble b: bubbleList) {
                b.update();
                b.checkCollisions();
            }

             */
            invalidate();
        }
    };
    protected void onDraw (Canvas canvas) {
        for (Bubble b: bubbleList) {
            b.draw(canvas);
        }
        handler.postDelayed(runnable, delay);
    }
    @Override
    public boolean onTouch(View v, MotionEvent motionEvent) {
        for (int i = 0; i < motionEvent.getPointerCount(); i++) {
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();
            int s = 2 * size;
            bubbleList.add(new Bubble(x, y, s));
        }
        return true;
    }


    private class Bubble {
        private int x;
        private int y;
        private int size;
        private int color;
        /*
        private int xspeed;
        private int yspeed;
        private final int MAX_SPEED = 15;
*/
        public Bubble (int newX, int newY, int newSize) {
            x    = newX;
            y    = newY;
            size = newSize;
            color = Color.argb(rand.nextInt(256),
                    rand.nextInt(256),
                    rand.nextInt(256),
                    rand.nextInt(256));

            /*xspeed = rand.nextInt(MAX_SPEED * 2) - MAX_SPEED;
            yspeed = rand.nextInt(MAX_SPEED * 2) - MAX_SPEED;
             */
        }
         public  void draw(Canvas canvas) {
            paint.setColor(color);
            canvas.drawOval(x - size / 2, y - size / 2,
                    x + size / 2, y + size / 2, paint);
        }
        /*public void update() {
            x += xspeed;
            y += yspeed;
        }
        public void checkCollisions () {
            if (x - size / 2 <= 0 || x + size / 2 >= getWidth()) {
                xspeed = -xspeed;
            }
            if (y - size / 2 <= 0 || y + size / 2 >= getHeight()) {
                yspeed = -yspeed;
            }
        }*/
    }

}


