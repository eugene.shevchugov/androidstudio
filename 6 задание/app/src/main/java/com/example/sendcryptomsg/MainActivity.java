package com.example.sendcryptomsg;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private EditText txtIn;
    private EditText txtOut;
    private Button btnEncode;


    public String encDec (String message) {
        char [] uppercase = new char[26];
        int indexUp       = 65;
        int i = 0;
        while (indexUp <= 90) {
            uppercase[i] = (char) indexUp;
            indexUp++;
            i++;
        }

        char [] lowercase = new char[26];
        int indexLow      = 97;
        int k = 0;
        while ( indexLow <= 122) {
            lowercase[k] = (char) indexLow;
            indexLow++;
            k++;

        }

        char  [] digit    = new char[10];
        int indexNum      = 48;
        int m = 0;
        while (indexNum <= 57) {
            digit[m] = (char) indexNum;
            indexNum++;
            m++;
        }

        String output = "";
        for (int j = 0; j < message.length(); j++) {
            for (int l = 0; l < 26; l++) {
                if (message.charAt(j) == uppercase[l]) {
                    output += uppercase[25 - l];
                    break;
                }
                else if (message.charAt(j) == lowercase[l]) {
                    output += lowercase[25 - l];
                    break;
                }
            }
            for (int l = 0; l < 10; l++) {
                if (message.charAt(j) == digit[l]) {
                    output += digit[9 - l];
                    break;
                }
            }
        }

        return output;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtIn     = (EditText) findViewById(R.id.txtIn);
        txtOut    = (EditText) findViewById(R.id.txtOut);
        btnEncode = (Button)   findViewById(R.id.btnEncode);

        Intent receivedIntent = getIntent();
        String receivedText   = receivedIntent.getStringExtra(Intent.EXTRA_TEXT);
        if (receivedText != null) {
            txtIn.setText(receivedText);
        }
        btnEncode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = txtIn.getText().toString();
                String output  = encDec(message);
                txtOut.setText(output);
            }
        });



        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shareIntent = new Intent (Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Secret message " + DateFormat.getDateTimeInstance().format(new Date()));
                shareIntent.putExtra(Intent.EXTRA_TEXT, txtOut.getText().toString());
                try {
                    startActivity(Intent.createChooser(shareIntent, "Share message..."));
                    finish();
                }
                catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(MainActivity.this, "Error: Couldn't share.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
